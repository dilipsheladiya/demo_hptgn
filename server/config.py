
import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    """Base configuration."""
    SECRET_KEY = 'sfwe434o798e7fyd@$*#(sdsd34@@#mdfke34l3m3@&#j'
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:root@localhost:5432/hptgn'
    BCRYPT_LOG_ROUNDS = 15
    
