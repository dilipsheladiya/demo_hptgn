import sys
from flask import session
from server import app
from server.models import Author
from helpers.authheader import check_authheader

class HelperView:
   
    @staticmethod
    def validate_session():
      
        error_message = None
        auth_token = check_authheader()
        if not auth_token and session.get('auth_token') is not None:
            auth_token = session['auth_token']
        app.logger.debug(auth_token)

        try:
            if not auth_token:
                error_message = 'Provide a Session Id'
            if auth_token:
                resp = Author.decode_auth_token(auth_token)
                if isinstance(resp, str):
                    error_message = resp
            if error_message:
                app.logger.info(error_message)
            return error_message
        except: 
            err = sys.exc_info()
            app.logger.exception("error in validate_session, %s", err)
            return err
