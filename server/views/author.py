from http import HTTPStatus
import re
import json
from flask import (Blueprint, request, make_response,
                   jsonify, session, redirect)
from flask.views import MethodView
from server import bcrypt, db, app
from server.views.helperview import HelperView
from server.models import Author

from helpers.userdata import create_user

AUTHOR_BLUEPRINT = Blueprint('author', __name__)

class CreateAuthor(MethodView):    
    def post(self):
         
        post_data = request.get_json(force=True)
        if 'username' not in post_data or bool(post_data.get('username')) is False:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'username is missing'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        username_match = re.match(
            r'(^[a-zA-Z0-9]([a-zA-Z0-9]){3,100}[a-zA-Z0-9]$)',
            post_data.get('username'))
        if username_match is None:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'Invalid username, only aplphanumeric of length 5 to 100 allowed'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        author = Author.query.filter_by(username=post_data.get('username')).first()
        if author is not None:
            response_object = {
                'status': 'FOUND',
                'message': 'User already exists'
            }
            return make_response(jsonify(response_object)), HTTPStatus.FOUND
        if 'password' not in post_data or bool(post_data.get('password')) is False:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'password is missing'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        if 'firstname' not in post_data or bool(post_data.get('firstname')) is False:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'firstname is missing'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        if 'lastname' not in post_data or bool(post_data.get('lastname')) is False:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'lastname is missing'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        
        res = create_user(post_data)
        if res == 0:
            response_object = {
                'status': 'OK',
                'message': 'User created successfully'
            }
            return make_response(jsonify(response_object)), HTTPStatus.OK
        else:
            response_object = {
            'status': 'INTERNAL_SERVER_ERROR',
            'message': json.dumps(res)
            }
            return make_response(jsonify(response_object)), HTTPStatus.INTERNAL_SERVER_ERROR            

CREATE_AUTHOR = CreateAuthor.as_view('create_author')
AUTHOR_BLUEPRINT.add_url_rule(
    '/demo/author/create',
    view_func=CREATE_AUTHOR,
    methods=['POST']
)





