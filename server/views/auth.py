
from http import HTTPStatus
import re
from flask import (Blueprint, request, make_response,
                   jsonify, session, redirect)
from flask.views import MethodView
from server import bcrypt, db, app
from server.models import Author, BlacklistToken
from helpers.authheader import check_authheader, get_username_pass
AUTH_BLUEPRINT = Blueprint('auth', __name__)

class LoginAPI(MethodView):    
    def post(self):
        '''Login POST method'''
        username, password = get_username_pass()
        try:
            if username is not None and password is not None:
                user = Author.query.filter_by(username=username).first()
                if user and bcrypt.check_password_hash(
                        user.password, password):
                    session['username'] = username
                    auth_token = user.encode_auth_token(user.id)
                    if auth_token:
                        session['auth_token'] = auth_token.decode()
                        
                        response_object = {
                            'status': 'OK',
                            'message': 'Successfully logged in.',
                            'session_id': auth_token.decode()
                        }
                        
                        return make_response(jsonify(response_object)), HTTPStatus.OK
                else:
                    response_object = {
                        'status': 'NOT_FOUND',
                        'message': ('Invalid user name or password')
                    }
                    app.logger.error(response_object)
                    return make_response(jsonify(response_object)), HTTPStatus.NOT_FOUND
            else:
                response_object = {
                    'status': 'UNAUTHORIZED',
                    'message': 'Unauthorized access'
                }
                return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
        except Exception as exc:
            app.logger.exception(exc)
            response_object = {
                'status': 'INTERNAL_SERVER_ERROR',
                'message': 'Internal server error, Try again'
            }
            return make_response(jsonify(response_object)), HTTPStatus.INTERNAL_SERVER_ERROR


class LogoutAPI(MethodView):
    def post(self):
        username = None
        if 'username' in session:
            username = session['username']
       
        auth_token = None
        if session.get('auth_token') is not None:
            auth_token = session['auth_token']
        if auth_token is not None:
            resp = Author.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                
                blacklist_token = BlacklistToken(token=auth_token)
                try:
                  
                    db.session.add(blacklist_token)
                    db.session.commit()
                   
                    response_object = {
                        'status': 'OK',
                        'message': 'Successfully logged out.'
                    }
                    return make_response(jsonify(response_object)), HTTPStatus.OK
                    return redirect('/')
                except Exception as exc:
                    response_object = {
                        'status': 'INTERNAL_SERVER_ERROR',
                        'message': exc
                    }
                    app.logger.exception(exc)
                    return make_response(jsonify(response_object)), HTTPStatus.INTERNAL_SERVER_ERROR
                    
            else:
                response_object = {
                    'status': 'UNAUTHORIZED',
                    'message': resp
                }
                app.logger.error(response_object)
                return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
               
        else:
            response_object = {
                'status': 'FORBIDDEN',
                'message': 'Provide a valid sessionid.'
            }
            app.logger.error(response_object)
            return make_response(jsonify(response_object)), HTTPStatus.FORBIDDEN
           
# define the API resources
LOGIN_VIEW = LoginAPI.as_view('login_api')
LOGOUT_VIEW = LogoutAPI.as_view('logout_api')

# add Rules for API Endpoints
AUTH_BLUEPRINT.add_url_rule(
    '/demo/login',
    view_func=LOGIN_VIEW,
    methods=['POST']
)
AUTH_BLUEPRINT.add_url_rule(
    '/demo/logout',
    view_func=LOGOUT_VIEW,
    methods=['POST']
)

