import json
import datetime
import re
import sys
from http import HTTPStatus 
from flask import (Blueprint, request, make_response,
                   jsonify, render_template, session, redirect)
from flask.views import MethodView
from server import db, app
from server.views.helperview import HelperView
from server.models import Book, Author
from webargs.flaskparser import use_kwargs

BOOKS_BLUEPRINT = Blueprint('books', __name__)

class CreateBook(MethodView):
    def post(self):
        session_error = HelperView.validate_session()
        if session_error:
            response_object = {
                'status': 'UNAUTHORIZED',
                'message': session_error
            }
            return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
        username = session['username']
        author = Author.query.filter_by(username=username).first()
        author_id = author.id
        post_data = request.get_json(force=True)
       
        if 'bookname' not in post_data or bool(post_data.get('bookname')) is False:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'bookname is missing'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        book = Book.query.filter_by(bookname=post_data.get('bookname')).first()
        if book is not None:
            response_object = {
                'status': 'FOUND',
                'message': 'Book already exists'
            }
            return make_response(jsonify(response_object)), HTTPStatus.FOUND
        res = self.create_book(post_data, author_id)
        if res == 0:
            response_object = {
                'status': 'OK',
                'message': 'Book created successfully'
            }
            return make_response(jsonify(response_object)), HTTPStatus.OK
        else:
            response_object = {
                'status': 'INTERNAL_SERVER_ERROR',
                'message': json.dumps(res)
            }
            return make_response(jsonify(response_object)), HTTPStatus.INTERNAL_SERVER_ERROR
    @staticmethod
    def create_book(post_data, author_id):
        bookname = post_data.get('bookname')
        if 'content' in post_data:
            content = post_data['content']
        try:
            book = Book(
                bookname=bookname,
                content=content,
                authorid=author_id,
            )
            db.session.add(book)
            db.session.commit()
            res = 0    
            
        except Exception as exc:
            app.logger.exception(str(exc))
            res = str(exc)
        return res


CREATE_BOOKS = CreateBook.as_view('create_book')

BOOKS_BLUEPRINT.add_url_rule(
    '/demo/book/create',
    view_func=CREATE_BOOKS,
    methods=['POST']
)

class Booklist(MethodView):
    
    def get(self):
        
        session_error = HelperView.validate_session()
        if session_error:
            response_object = {
                'status': 'UNAUTHORIZED',
                'message': session_error
            }
            return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
        #========================================================================
        username = session['username']
        author = Author.query.filter_by(username=username).first()
        author_id = author.id
        res = self.booklist(author_id)
        return make_response(jsonify(res)), HTTPStatus.OK

    @staticmethod
    def booklist(author_id):
        book_list = Book.query.filter_by(authorid=author_id).order_by(Book.id.asc()).all()
        blist = list()
        i = 1
        for book in book_list:
            try:
                val = {
                    'sno':i,
                    'bookid': book.id,
                    'bookname': book.bookname,
                    
                }
                blist.append(val)
                i = i + 1
            except Exception as exc:
                app.logger.exception(str(exc))
                
        return blist

BOOK_LIST = Booklist.as_view('book_list')

BOOKS_BLUEPRINT.add_url_rule(
    '/demo/book/list',
    view_func=BOOK_LIST,
    methods=['GET']
)


class Bookdetail(MethodView):    
    def get(self, bookid):
        print(bookid)
        session_error = HelperView.validate_session()
        if session_error:
            response_object = {
                'status': 'UNAUTHORIZED',
                'message': session_error
            }
            return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
        
        username = session['username']
        book = Book.query.filter_by(id=bookid).first()
        if book is None:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'provide a valid book id'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        author = Author.query.filter_by(username=username).first()
        author_id = author.id
        
        book_detail = Book.query.filter_by(authorid=author_id, id=bookid).first()
        book_detail_data = {
            'bookname': book_detail.bookname,
            'content': book_detail.content,
            'Author':book_detail.author.firstname
        }
        return make_response(jsonify(book_detail_data)), HTTPStatus.OK
BOOK_DETAIL = Bookdetail.as_view('book_detail')
BOOKS_BLUEPRINT.add_url_rule(
    '/demo/bookdetail/<bookid>',
    view_func=BOOK_DETAIL,
    methods=['GET']
)
        

class UpdateBook(MethodView):
    def put(self, bookid):
        session_error = HelperView.validate_session()
        if session_error:
            response_object = {
                'status': 'UNAUTHORIZED',
                'message': session_error
            }
            return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
        #========================================================================
        username = session['username']
        book = Book.query.filter_by(id=bookid).first()
        if book is None:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'provide a valid book id'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        author = Author.query.filter_by(username=username).first()
        author_id = author.id
        post_data = request.get_json(force=True)
        
        new_b = Book.query.filter_by(id=bookid).first()
        new_book = Book.query.filter(Book.bookname == post_data.get('bookname'), Book.bookname != new_b.bookname).first()
        if new_book is not None:
            response_object = {
                'status': 'FOUND',
                'message': 'Book you enter is already exists'
            }
            return make_response(jsonify(response_object)), HTTPStatus.FOUND
        if 'bookname' not in post_data or bool(post_data.get('bookname')) is False:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'bookname is missing to update the book'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        
        res = self.update_book(post_data, author_id, bookid)
        
        if res == 0:
            response_object = {
                'status': 'OK',
                'message': 'Book updated successfully'
            }
            return make_response(jsonify(response_object)), HTTPStatus.OK
        else:
            response_object = {
                'status': 'INTERNAL_SERVER_ERROR',
                'message': json.dumps(res)
            }
            return make_response(jsonify(response_object)), HTTPStatus.INTERNAL_SERVER_ERROR
    @staticmethod
    def update_book(post_data, author_id, bookid):
        
        try:
            if 'content' in post_data:
                content = post_data['content']
            if 'bookname' in post_data:
                bookname = post_data['bookname']
            book_obj = Book.query.filter_by(authorid = author_id, id=bookid).first()
            
            book_obj.bookname = bookname

            book_obj.content = content
            db.session.commit()
            res = 0
        except Exception as exc:
            
            app.logger.exception(str(exc))
            res = str(exc)
        return res


UPDATE_BOOK = UpdateBook.as_view('update_book')

BOOKS_BLUEPRINT.add_url_rule(
    '/demo/book/update/<bookid>',
    view_func=UPDATE_BOOK,
    methods=['PUT']
)

class DeleteBook(MethodView):
   
    def delete(self, bookid):
        
        session_error = HelperView.validate_session()
        if session_error:
            response_object = {
                'status': 'UNAUTHORIZED',
                'message': session_error
            }
            return make_response(jsonify(response_object)), HTTPStatus.UNAUTHORIZED
        
        username = session['username']
        book = Book.query.filter_by(id=bookid).first()
        if book is None:
            response_object = {
                'status': 'NOT_ACCEPTABLE',
                'message': 'provide a valid book id'
            }
            return make_response(jsonify(response_object)), HTTPStatus.NOT_ACCEPTABLE
        author = Author.query.filter_by(username=username).first()
        author_id = author.id
        
        res = self.delete_book(bookid, author_id)

        if res == 0:
            response_object = {
                'status': 'OK',
                'message': 'Book deleted successfully'
            }
            
            return make_response(jsonify(response_object)), HTTPStatus.OK
        else:
            response_object = {
                'status': 'INTERNAL_SERVER_ERROR',
                'message': json.dumps(res)
            }
            return make_response(jsonify(response_object)), HTTPStatus.INTERNAL_SERVER_ERROR
    @staticmethod
    def delete_book(bookid, author_id):
        try:
            Book.query.filter_by(id=bookid, authorid=author_id).delete()
            db.session.commit()
            res = 0
        except Exception as exc:
            app.logger.exception(str(exc))
            res = str(exc)
        return res

DELETE_BOOK = DeleteBook.as_view('delete_book')

BOOKS_BLUEPRINT.add_url_rule(
    '/demo/book/delete/<bookid>',
    view_func=DELETE_BOOK,
    methods=['DELETE']
)

