import datetime
import jwt
from server import app, db, bcrypt

class Book(db.Model):
    __tablename__ = "books"

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    bookname = db.Column(db.String(255), unique=True, nullable=True)
    content = db.Column(db.String(255), nullable=True)
    authorid = db.Column(db.Integer, db.ForeignKey('author.id'), nullable=True)    
    author = db.relationship('Author')

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        self.bookname = self.kwargs['bookname']
        if 'content' in self.kwargs:
            self.content = self.kwargs['content']
        self.authorid = kwargs['authorid']

class Author(db.Model):
    __tablename__ = "author"

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    username = db.Column(db.String(255), unique=True, nullable=True)
    password = db.Column(db.String(255), nullable=True)
    firstname = db.Column(db.String(255), nullable=True)
    lastname = db.Column(db.String(255), nullable=True)

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        self.username = self.kwargs['username']
        self.firstname = self.kwargs['firstname']
        self.lastname = self.kwargs['lastname']
        if self.kwargs['password'] is not None:
            self.password = bcrypt.generate_password_hash(
                self.kwargs['password'], app.config.get('BCRYPT_LOG_ROUNDS')
            ).decode()
        

    @staticmethod
    def encode_auth_token(user_id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(
                    days=0, seconds=3600),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                app.config.get('SECRET_KEY'),
                algorithm='HS512'
            )
        except Exception as exc:
            return exc

    @staticmethod
    def decode_auth_token(auth_token):
        try:
            payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'


class BlacklistToken(db.Model):
    __tablename__ = 'blacklist_tokens'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    blacklisted_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.datetime.now()

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False
