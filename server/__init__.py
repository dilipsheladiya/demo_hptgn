import os
import logging
import logging.config
from flask import Flask, jsonify, make_response
from flask_limiter import Limiter
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from server.config import Config
logging.config.fileConfig('logging.conf')
logger = logging.getLogger('Demo_heptagon')

app = Flask(__name__)
app.config.from_object(Config)
bcrypt = Bcrypt(app)
db = SQLAlchemy(app)
from server import  models

@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    '''function handle_error'''
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    return jsonify({"errors": messages}), err.code

from server.views import (auth, author, books)
app.register_blueprint(auth.AUTH_BLUEPRINT)
app.register_blueprint(books.BOOKS_BLUEPRINT)
app.register_blueprint(author.AUTHOR_BLUEPRINT)
