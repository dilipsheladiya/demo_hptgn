from __future__ import print_function
import datetime
from flask import session
from server import bcrypt, db, app
from server.models import Author


def create_user(post_data):
    
    username = post_data.get('username')
    firstname = post_data.get('firstname')
    lastname = post_data.get('lastname')
    password = post_data.get('password')    
    try:
        author = Author(
            username=username,
            password=password,
            firstname=firstname,
            lastname=lastname,
        )
        db.session.add(author)
        db.session.commit()
        res = 0       
    except Exception as exc:
        app.logger.exception(str(exc))
        res = str(exc)
    return res
