from flask import request

def check_authheader():
    
    auth_token = ''
    auth_header = request.headers.get('Authorization')
    if auth_header and len(auth_header.split(' ')) > 1:
        auth_token = auth_header.split(' ')[1]

    return auth_token


def get_username_pass():
   
    username = password = None

    auth = request.authorization

    if auth:
        if auth.username:
            username = auth.username

        if auth.password:
            password = auth.password

    return username, password
