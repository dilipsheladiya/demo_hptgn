
import sys
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from server import app, db

MIGRATE = Migrate(app, db)
MANAGER = Manager(app)

# migrations
MANAGER.add_command('db', MigrateCommand)



if __name__ == '__main__':
    MANAGER.run()
