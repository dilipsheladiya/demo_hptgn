1. create virtual environment in root directory, Activate  it and install all library using requirements.txt
2. create database with name "hptgn" in postgresql
3. then go to directroy where manage.py file available then run following command:
    - python manage.py db init
    - python manage.py db migrate
    - python manage.py db upgrade
4. then  run below command  from root directory
    - set FLASK_APP=main.py
    - set FLASK_DEBUG=1
    - flask run
5. API  - befor api use chnage proxy setting : refer - proxy_setting_for_local.PNG image from mail
    1. author create api.
        method: POST
        API: http://localhost:5000/demo/author/create
        request body:
            {
                "username":"<username>",
                "password":"<password>",
                "firstname":"<firstname>",
                "lastname":"<lastname>"
            }
        also refer screenshot in mail : post_request_for_author_create.PNG
    2. author login api.
        method: POST
        API: http://localhost:5000/demo/login
        Authorization: from Authorization select basic auth and enter your register credentials 
        on success you will get session_id in response then use this session_id as Authorization token in all api   
        also refer screenshot in mail : post_request_for_author_login.PNG
    3. author logout api.
        method: POST
        API: http://localhost:5000/demo/logout
        Authorization: from Authorization select bearer token and add session_id as token from "author login api"
        also refer screenshot in mail : post_request_for_author_logout.PNG
    4. Book create api.
        method: POST
        API: http://localhost:5000/demo/book/create
        Authorization: from Authorization select bearer token and add session_id as token from "author login api"
        request body:
            {
                "bookname":"<bookname>",
                "content":"<content>"
            }
        also refer screenshot in mail : post_request_for_book_create.PNG
    5. Book list api.
        method: GET
        API: http://localhost:5000/demo/book/list
        Authorization: from Authorization select bearer token and add session_id as token from "author login api"
        
        also refer screenshot in mail : get_request_for_book_list.PNG
    6. Book detail api.
        method: GET
        API: http://localhost:5000/demo/bookdetail/<bookid>
        where <bookid> is id of book
        EX...http://localhost:5000/demo/bookdetail/10
        Authorization: from Authorization select bearer token and add session_id as token from "author login api"
        
        also refer screenshot in mail : get_request_for_book_detail.PNG
    7. Book update api.
        method: PUT
        API: http://localhost:5000/demo/book/update/<bookid>
        where <bookid> is id of book
        EX...http://localhost:5000/demo/book/update/10
        Authorization: from Authorization select bearer token and add session_id as token from "author login api"
        
        also refer screenshot in mail : put_request_for_book_update_book.PNG
    8. Book delete api.
        method: PUT
        API: http://localhost:5000/demo/book/delete/<bookid>
        where <bookid> is id of book
        EX...http://localhost:5000/demo/book/delete/10
        Authorization: from Authorization select bearer token and add session_id as token from "author login api"
        
        also refer screenshot in mail : delete_request_for_delete_book.PNG



    for emmail send
        goto \mailsend folder
        then run python mail.py use your gmail credentials.
        alos make https://myaccount.google.com/u/1/lesssecureapps?pli=1 this ON